import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  variant: PropTypes.string,
};

const defaultProps = {
  size: 24,
  variant: "default",
  color: "var(--secondary)",
};

const Active = ({ size, variant, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12 8C13.1046 8 14 7.10457 14 6C14 4.89543 13.1046 4 12 4C10.8954 4 10 4.89543 10 6C10 7.10457 10.8954 8 12 8ZM12 10C14.2091 10 16 8.20914 16 6C16 3.79086 14.2091 2 12 2C9.79086 2 8 3.79086 8 6C8 8.20914 9.79086 10 12 10ZM13.2941 14.1704C12.5533 13.9719 11.7767 13.9465 11.0245 14.0961C10.2723 14.2457 9.56465 14.5664 8.95619 15.0332C8.34774 15.5001 7.85484 16.1007 7.51564 16.7886C7.17643 17.4764 7 18.2331 7 19C7 19.5523 6.55228 20 6 20C5.44772 20 5 19.5523 5 19C5 17.9263 5.247 16.867 5.72189 15.904C6.19678 14.941 6.88684 14.1002 7.73867 13.4465C8.5905 12.7929 9.58128 12.344 10.6344 12.1345C11.6874 11.925 12.7746 11.9606 13.8117 12.2385C14.3452 12.3815 14.6618 12.9298 14.5188 13.4633C14.3759 13.9967 13.8276 14.3133 13.2941 14.1704ZM20.7071 13.7071C21.0976 13.3166 21.0976 12.6834 20.7071 12.2929C20.3166 11.9024 19.6834 11.9024 19.2929 12.2929L14 17.5858L12.7071 16.2929C12.3166 15.9024 11.6834 15.9024 11.2929 16.2929C10.9024 16.6834 10.9024 17.3166 11.2929 17.7071L13.2929 19.7071C13.6834 20.0976 14.3166 20.0976 14.7071 19.7071L20.7071 13.7071Z"
      fill={color}
    />
  </svg>
);

Active.propTypes = propTypes;
Active.defaultProps = defaultProps;

export default Active;
