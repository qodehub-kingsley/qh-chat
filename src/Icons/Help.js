import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  variant: PropTypes.string,
};

const defaultProps = {
  size: 24,
  variant: "default",
  color: "var(--secondary)",
};

const Help = ({ size, variant, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12ZM22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM13.3566 7.50629L13.0699 7.41073C12.3754 7.17923 11.6246 7.17923 10.9301 7.41073C10.3747 7.59588 10 8.11568 10 8.70117V9C10 9.55229 9.55228 10 9 10C8.44772 10 8 9.55229 8 9V8.70117C8 7.25482 8.92551 5.97074 10.2976 5.51336C11.4027 5.14502 12.5973 5.14502 13.7024 5.51336L13.989 5.60892C15.14 5.99258 16.0152 6.93767 16.3094 8.11468C16.4487 8.67176 16.4719 9.25154 16.3775 9.81795L16.3403 10.0408C16.0641 11.6979 14.6646 12.9256 13 12.9967V14C13 14.5523 12.5523 15 12 15C11.4477 15 11 14.5523 11 14V12.8471C11 11.827 11.827 11 12.8471 11C13.6006 11 14.2437 10.4553 14.3675 9.71202L14.4047 9.48916C14.454 9.19339 14.4419 8.89064 14.3692 8.59975C14.2408 8.08613 13.8588 7.67371 13.3566 7.50629ZM13 17C13 17.5523 12.5523 18 12 18C11.4477 18 11 17.5523 11 17C11 16.4477 11.4477 16 12 16C12.5523 16 13 16.4477 13 17Z"
      fill={color}
    />
  </svg>
);

Help.propTypes = propTypes;
Help.defaultProps = defaultProps;

export default Help;
