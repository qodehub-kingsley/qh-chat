import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  variant: PropTypes.string,
};

const defaultProps = {
  size: 24,
  variant: "default",
  color: "var(--secondary)",
};

const User = ({ size, variant, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M11.9999 11C14.2091 11 15.9999 9.20914 15.9999 7C15.9999 4.79086 14.2091 3 11.9999 3C9.79079 3 7.99992 4.79086 7.99992 7C7.99992 9.20914 9.79079 11 11.9999 11ZM19.9376 20.0026C20.0065 20.5505 19.5523 21 19 21H12L5.00001 21C4.44773 21 3.99356 20.5505 4.0624 20.0026C4.28277 18.2486 5.07991 16.6064 6.34316 15.3431C7.84345 13.8429 9.87828 13 12 13C14.1217 13 16.1566 13.8429 17.6569 15.3431C18.9201 16.6064 19.7173 18.2486 19.9376 20.0026Z"
      fill={color}
    />
  </svg>
);

User.propTypes = propTypes;
User.defaultProps = defaultProps;

export default User;
