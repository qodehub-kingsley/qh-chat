import React from "react";
import Avatar from "@material-ui/core/Avatar";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import OnlineIcon from "@material-ui/icons/FiberManualRecord";
import OfflineIcon from "@material-ui/icons/FiberManualRecordOutlined";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import AccessTimeIcon from "@material-ui/icons/AccessTimeOutlined";
import SearchIcon from "@material-ui/icons/Search";
import HelpIcon from "@material-ui/icons/HelpOutline";
import styled from "styled-components";

function Index(props) {
  const { user } = props;

  return (
    <Header className="header">
      <hgroup>
        <h2>Logo</h2>
      </hgroup>

      <main className="header__main">
        <ArrowBackIcon color="disabled" className="hide-mobile" />
        <ArrowForwardIcon className="hide-mobile" />
        <AccessTimeIcon />

        <div className="header__search">
          <SearchIcon />
          <input placeholder="Search Sklack" />
        </div>
        <HelpIcon />
      </main>

      <aside className="header__user">
        <Avatar className="header__user__icon" />
        <div className="header__user--online">
          {!user?.isOnline ? (
            <OnlineIcon
              style={{ color: "var(--green)" }}
              fontSize="small"
              className="header__user--online"
            />
          ) : (
            <OfflineIcon />
          )}
        </div>
      </aside>
    </Header>
  );
}

const Header = styled.header`
  // layout
  display: flex;
  padding: 10px;
  align-items: center;
  justify-content: space-between;

  // appearance/colors
  background: var(--slack-color);
  color: white;

  // elements
  .header__main {
    flex: 0.5;
    display: flex;
    cursor: pointer;
    justify-content: space-between;
  }

  .header__user {
    position: relative;

    .header__user__icon {
      border-radius: 4px;
      // border-bottom-right-radius: 1rem;
    }

    .header__user--online {
      // appearance
      background: var(--slack-color);
      border-radius: 1.5rem;
      position: absolute;
      padding-left: 2px;
      padding-top: 2px;
      display: flex;
      bottom: -4px;
      right: -4px;
    }
  }

  .header__search {
    flex: 0.8;
    display: flex;
    text-align: center;
    // justify-content: center;
    align-items: center;

    // spacing
    padding: 0 50px;

    // appearance
    background-color: var(--slack-color--light);
    box-shadow: var(--slack-box-shadow--light);
    border-radius: 6px;
    color: gray;
    transition: 0.6ms;

    &:hover {
      background-color: var(--slack-color--medium);
      box-shadow: var(--slack-box-shadow--medium);
      color: white;

      input {
        ::placeholder {
          color: white;
        }
      }
    }

    input {
      background-color: transparent;
      color: white;
      border: none;
      width: 100%;
      outline: 0;
    }
  }

  @media screen and (max-width: 600px) {
    .hide-mobile {
      display: none;
    }
  }
`;

export default Index;
