import React from "react";
import { Sidebar } from "./Sidebar";
import styled from "styled-components";

function Index() {
  return (
    <Content className="main">
      <Sidebar />
    </Content>
  );
}

const Content = styled.main`
  // appearance
  height: 100%;

  // allow child elements to fill full height
  & > * {
    height: 100%;
  }
`;

export default Index;
