import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.component,
  children: PropTypes.any,
};

function SidebarAction({ text, icon, children, ...props }) {
  return (
    <div className={`sidebar-action ${props.className ?? ""}`}>
      <div>{(icon || children) ?? "#"}</div>
      <p>{text} </p>
    </div>
  );
}

SidebarAction.propTypes = propTypes;

export default SidebarAction;
