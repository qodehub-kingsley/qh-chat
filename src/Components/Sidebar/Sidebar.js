import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import SidebarAction from "./SidebarAction";
import ForumIcon from "@material-ui/icons/Forum";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import InsertCommentIcon from "@material-ui/icons/InsertComment";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
import CreateIcon from "@material-ui/icons/Create";
// import CreateIcon from "@material-ui/icons/CreateOutlined";

const propTypes = {
  company: PropTypes.object.isRequired,
};

function Sidebar(props) {
  const { company } = props;

  return (
    <Side className="sidebar">
      <div className="sidebar__header">
        <div className="sidebar__company">
          <h2 className="sidebar__company__name">
            {company?.name || "Sklack"}
          </h2>
        </div>
        <CreateIcon />
      </div>

      <div className="sidebar__content">
        <SidebarAction
          icon={<InsertCommentIcon />}
          className="icon"
          text="Threads"
        />
        <SidebarAction icon={<ForumIcon />} text="All DMs" />
        <SidebarAction
          icon={<AlternateEmailIcon />}
          text="Mentions & reactions"
        />
        <SidebarAction text="Saved items">
          <BookmarkBorderIcon />
        </SidebarAction>
        <SidebarAction icon={<MoreVertIcon />} text="More" />

        <div className="sidebar-chats">
          <div className="sidebar__sidebar-chat__group">
            <SidebarAction text="Starred">
              <ArrowRightIcon />
            </SidebarAction>
          </div>
        </div>
      </div>
    </Side>
  );
}

const Side = styled.aside`
  // appearance/colors
  background: var(--slack-color);

  // layout
  align-items: baseline;
  max-width: 260px;
  padding: 10px;
  color: white;

  .sidebar__header {
    display: flex;
    padding: 10px;
    cursor: pointer;
    align-items: center;
    justify-content: space-between;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    border-color: rgb(82, 38, 83);
  }

  .sidebar__content {
    padding: 10px 0;

    > * {
      margin-top: 10px;
      margin-bottom: 10px;
    }
  }

  .sidebar-action {
    display: flex;
    cursor: pointer;
    padding: 10px 0px;
    align-items: center;

    :hover {
      background: var(--slack-color--dark);
    }

    & > *:first-child {
      width: 24px;
      display: flex;
      font-size: 1.5rem;
      margin-right: 15px;
    }
  }
`;

Sidebar.propTypes = propTypes;

export default Sidebar;
